/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"io"
)

type ConnackMessage struct {
	Header
	ReturnCode byte
}

func (c *ConnackMessage) MessageType() byte {
	return CONNACK
}

func (c *ConnackMessage) Write(w io.Writer) error {
	packet := c.Header.pack()
	packet.WriteByte(CONNACK)
	packet.WriteByte(c.ReturnCode)
	_, err := packet.WriteTo(w)

	return err
}

func (c *ConnackMessage) Unpack(b io.Reader) {
	c.ReturnCode = readByte(b)
}
