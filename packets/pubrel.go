/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"io"
)

type PubrelMessage struct {
	Header
	MessageId uint16
}

func (p *PubrelMessage) MessageType() byte {
	return PUBREL
}

func (p *PubrelMessage) Write(w io.Writer) error {
	packet := p.Header.pack()
	packet.WriteByte(PUBREL)
	packet.Write(encodeUint16(p.MessageId))
	_, err := packet.WriteTo(w)

	return err
}

func (p *PubrelMessage) Unpack(b io.Reader) {
	p.MessageId = readUint16(b)
}
