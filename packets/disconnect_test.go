/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestDisconnectStruct(t *testing.T) {
	msg := NewMessage(DISCONNECT).(*DisconnectMessage)

	if assert.NotNil(t, msg, "New message should not be nil") {
		assert.Equal(t, "*packets.DisconnectMessage", reflect.TypeOf(msg).String(), "Type should be DisconnectMessage")
		assert.Equal(t, 0, msg.Duration, "Default Duration should be 0")

		assert.Equal(t, DISCONNECT, msg.MessageType(), "MessageType() should return DISCONNECT")
	}
}
