/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"io"
)

type WillTopicRespMessage struct {
	Header
	ReturnCode byte
}

func (wt *WillTopicRespMessage) MessageType() byte {
	return WILLTOPICRESP
}

func (wt *WillTopicRespMessage) Write(w io.Writer) error {
	packet := wt.Header.pack()
	packet.WriteByte(WILLTOPICRESP)
	packet.WriteByte(wt.ReturnCode)
	_, err := packet.WriteTo(w)

	return err
}

func (wt *WillTopicRespMessage) Unpack(b io.Reader) {
	wt.ReturnCode = readByte(b)
}
