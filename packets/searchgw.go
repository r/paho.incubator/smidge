/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"io"
)

type SearchGwMessage struct {
	Header
	Radius byte
}

func (s *SearchGwMessage) MessageType() byte {
	return SEARCHGW
}

func (s *SearchGwMessage) Write(w io.Writer) error {
	packet := s.Header.pack()
	packet.WriteByte(SEARCHGW)
	packet.WriteByte(s.Radius)
	_, err := packet.WriteTo(w)

	return err
}

func (s *SearchGwMessage) Unpack(b io.Reader) {
	s.Radius = readByte(b)
}
