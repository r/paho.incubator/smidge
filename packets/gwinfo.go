/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"io"
)

type GwInfoMessage struct {
	Header
	GatewayId      byte
	GatewayAddress []byte
}

func (g *GwInfoMessage) MessageType() byte {
	return GWINFO
}

func (g *GwInfoMessage) Write(w io.Writer) error {
	g.Header.Length = uint16(len(g.GatewayAddress) + 3)
	packet := g.Header.pack()
	packet.WriteByte(GWINFO)
	packet.WriteByte(g.GatewayId)
	packet.Write(g.GatewayAddress)
	_, err := packet.WriteTo(w)

	return err
}

func (g *GwInfoMessage) Unpack(b io.Reader) {
	g.GatewayId = readByte(b)
	if g.Header.Length > 3 {
		b.Read(g.GatewayAddress)
	}
}
