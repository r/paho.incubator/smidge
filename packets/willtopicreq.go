/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"io"
)

type WillTopicReqMessage struct {
	Header
}

func (wt *WillTopicReqMessage) MessageType() byte {
	return WILLTOPICREQ
}

func (wt *WillTopicReqMessage) Write(w io.Writer) error {
	packet := wt.Header.pack()
	packet.WriteByte(WILLTOPICREQ)
	_, err := packet.WriteTo(w)

	return err
}

func (wt *WillTopicReqMessage) Unpack(b io.Reader) {

}
