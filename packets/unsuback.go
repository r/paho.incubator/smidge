/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"io"
)

type UnsubackMessage struct {
	Header
	MessageId uint16
}

func (u *UnsubackMessage) MessageType() byte {
	return UNSUBACK
}

func (u *UnsubackMessage) Write(w io.Writer) error {
	packet := u.Header.pack()
	packet.WriteByte(UNSUBACK)
	packet.Write(encodeUint16(u.MessageId))
	_, err := packet.WriteTo(w)

	return err
}

func (u *UnsubackMessage) Unpack(b io.Reader) {
	u.MessageId = readUint16(b)
}
