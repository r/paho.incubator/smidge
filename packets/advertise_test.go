/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package packets

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestAdvertiseStruct(t *testing.T) {
	msg := NewMessage(ADVERTISE).(*AdvertiseMessage)

	if assert.NotNil(t, msg, "New message should not be nil") {
		assert.Equal(t, "*packets.AdvertiseMessage", reflect.TypeOf(msg).String(), "Type should be AdvertiseMessage")
		assert.Equal(t, 0, msg.GatewayId, "Default GatewayId should be 0")
		assert.Equal(t, 0, msg.Duration, "Default Duration should be 0")
		assert.Equal(t, 5, msg.Length, "Length should be 5")

		assert.Equal(t, ADVERTISE, msg.MessageType(), "MessageType() should return ADVERTISE")
	}
}
