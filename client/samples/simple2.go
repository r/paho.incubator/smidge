/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package main

import (
	"fmt"
	"git.eclipse.org/gitroot/paho.incubator/smidge.git/client"
	"git.eclipse.org/gitroot/paho.incubator/smidge.git/packets"
	"log"
	"os"
	"time"
)

func main() {
	smidge.DEBUG = log.New(os.Stdout, "", 0)
	c, err := smidge.NewClient("udp://127.0.0.1:1884", "gotestclient2")
	if err != nil {
		panic(err)
	}
	//c.SetWill("testwill", 1, true, []byte("Will test"))
	ct := c.Connect()
	ct.Wait()
	fmt.Println(ct.ReturnCode)
	c.DefaultMessageHandler = func(c *smidge.SNClient, m *packets.PublishMessage) {
		fmt.Println(string(m.Data))
	}
	// rt := c.Register("gotest")
	// rt.WaitTimeout(10 * time.Second)
	// fmt.Println(rt.TopicName, rt.TopicId, rt.ReturnCode)
	st := c.Subscribe("#", 0, nil)
	st.WaitTimeout(10 * time.Second)
	fmt.Println(st.TopicName, st.Qos, st.TopicId, st.ReturnCode)
	// pt := c.Publish("gotest", 1, false, []byte("Hello smidge"))
	// pt.WaitTimeout(10 * time.Second)
	// fmt.Println(pt.TopicId, pt.ReturnCode)
	time.Sleep(3 * time.Second)
}
