/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation
 *******************************************************************************/

package smidge

import (
	P "git.eclipse.org/gitroot/paho.incubator/smidge.git/packets"
	"time"
)

type MessageAndToken struct {
	m P.Message
	t Token
}

type Token interface {
	Wait()
	WaitTimeout(time.Duration)
	flowComplete()
}

type baseToken struct {
	complete chan struct{}
	ready    bool
	err      error
}

// Wait will wait indefinitely for the Token to complete, ie the Publish
// to be sent and confirmed receipt from the broker
func (b *baseToken) Wait() {
	if !b.ready {
		<-b.complete
		b.ready = true
	}
}

// WaitTimeout takes a time in ms
func (b *baseToken) WaitTimeout(d time.Duration) {
	if !b.ready {
		select {
		case <-b.complete:
			b.ready = true
		case <-time.After(d):
		}
	}
}

func (b *baseToken) flowComplete() {
	close(b.complete)
}

func newToken(tType byte) Token {
	switch tType {
	case P.CONNECT:
		return &ConnectToken{baseToken: baseToken{complete: make(chan struct{})}}
	case P.REGISTER:
		return &RegisterToken{baseToken: baseToken{complete: make(chan struct{})}}
	case P.SUBSCRIBE:
		return &SubscribeToken{baseToken: baseToken{complete: make(chan struct{})}}
	case P.PUBLISH:
		return &PublishToken{baseToken: baseToken{complete: make(chan struct{})}}
	case P.UNSUBSCRIBE:
		return &UnsubscribeToken{baseToken: baseToken{complete: make(chan struct{})}}
	case P.WILLTOPIC, P.WILLMSG, P.WILLMSGUPD, P.WILLTOPICUPD:
		return &WillToken{baseToken: baseToken{complete: make(chan struct{})}}
	}
	return nil
}

type ConnectToken struct {
	baseToken
	ReturnCode byte
}

type RegisterToken struct {
	baseToken
	TopicName  string
	TopicId    uint16
	ReturnCode byte
}

type PublishToken struct {
	baseToken
	TopicId    uint16
	ReturnCode byte
}

type SubscribeToken struct {
	baseToken
	handler    MessageHandler
	topicType  byte
	Qos        byte
	TopicName  string
	TopicId    uint16
	ReturnCode byte
}

type UnsubscribeToken struct {
	baseToken
}

type WillToken struct {
	baseToken
}
