/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander
 *    Seth Hoenig
 *******************************************************************************/

package gateway

func validateClientId(clientid []byte) (string, error) {
	if len(clientid) == 0 {
		ERROR.Println("zero length client id not allowed")
		return "", ErrZeroLengthClientID
	}
	if len(clientid) > 23 {
		ERROR.Println("client id longer than 23 characters")
		return "", ErrClientIDTooLong
	}
	return string(clientid), nil
}
