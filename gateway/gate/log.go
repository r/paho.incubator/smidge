/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander
 *    Seth Hoenig
 *******************************************************************************/

package gateway

import (
	"io"
	"log"
)

var (
	INFO  *log.Logger
	ERROR *log.Logger
)

func InitLogger(infoHandle, errorHandle io.Writer) {
	INFO = log.New(infoHandle, "INFO:  ", log.Ldate|log.Ltime)
	ERROR = log.New(errorHandle, "ERROR: ", log.Ldate|log.Ltime)
}
