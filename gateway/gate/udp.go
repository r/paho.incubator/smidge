/*******************************************************************************
 * Copyright (c) 2014-2015 IBM Corp.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Allan Stockdill-Mander
 *    Seth Hoenig
 *******************************************************************************/

package gateway

import (
	"fmt"
	"net"
)

func port2str(port int) string {
	return fmt.Sprintf(":%d", port)
}

func listen(g Gateway) {
	address, err := net.ResolveUDPAddr("udp", port2str(g.Port()))
	chkerr(err)
	udpconn, err := net.ListenUDP("udp", address)
	chkerr(err)
	for {
		buffer := make([]byte, 1024)
		n, remote, err := udpconn.ReadFromUDP(buffer)
		chkerr(err)
		go g.OnPacket(n, buffer, udpconn, remote)
	}
}
